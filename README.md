# CRISP-view Annotation File Check
When you are done with a CRISPR screening paper analysis, and have prepared all of the annotation files about this paper, then you are ready to insert your result to the CRISP-view server. However, before you insert the data to the server you should check the format of the annotation files first, to ensure the success of the insertion.

Download the file [here.](https://bitbucket.org/weililab/crisp-view-annotation-files-check/downloads/AnnotationFileCheck.py)
## Features
This script corrects common errors within the annotation files including ***sample.txt, library.txt, publication.txt*** and checks for necessary files within the folder structure such as ***rawcount and fastq***.

- Removes commas from numbers
- Removes quotes
- Removes special characters (Non-ACII)
- Counts the number of special character errors & notifies the user
- Removes redundant rows
- Counts the number of redundant rows & notifies the user
- Modifies capital letters in “Type” to be lowercase
- Notifies the user if the directory in the sample.txt file is incorrect, specifically checks for the subfolder name within the sample.txt file
- Checks the “lib/” folder for the “library.csv” file
- Checks the “rawcount/” folder for the “rawcount.txt” file; if not found, checks for the “fastq” file
- Checks the “results/” folder for the “mle.vispr.yaml” file

## Prints to Console
The file will print a 0 or 1 along with the error message, indicating whether or not there is an error. For example:

    Sample Errors:

    1: No Comma Error
    0: Quotation Error
    1: No Capitalization Error in Type
    
    Special Character Error: 3
    redudant rows: 1
    0: Incorrect Directory Error
    ---------------------
    Results Folder Errors:
    1: No Results Folder Error, mle.vispr.yaml File Found
    ---------------------
    General File Errors:
    
    1: No Rawcount Error, Rawcount File Found
    1: No Library Error, Library File Found


## Creates New Files
The script creates three new files, ***sampleFixed.txt***, ***libraryFixed.txt***, and ***publicationFixed.txt***  which are error-eliminated versions of the the original annotation files.

## Parameters
| Parameter | Result |
| ------ | ------ |
| s | Fixes errors in the “sample.txt” file, creates the new & fixed “sampleFixed.txt” file. Prints errors to console. |
| l | Fixes errors in the “library.txt” file, creates the new & fixed “libraryFixed.txt” file. Prints errors to console. |
| p | Fixes errors in the “publication.txt” file, creates the new & fixed “publicationFixed.txt” file. Prints errors to console. |
| r | Checks the “results/” folder if the “mle.vispr.yaml” file exists. Prints whether or not it exists to console. |
| g | Checks the “rawcount/” folder if the “rawcount.txt” file exists. If it does not, checks where the “fastq” file exists. If neither are found, prints an error to the console. Checks the “lib/” folder if the “library.csv” exists. Prints whether or not it exists to console. |

## Usage
### Bash File Demo
To see a demonstration of the script, head to the desired subfolder within the "Demos" folder, and run the ***demo.sh*** file using the following syntax:
    
    $ chmod u+x demo.sh
    $./demo.sh
### Non-Demo Use
To install and use this script, place it in the subfolder folder along with the “annotation”, “lib”, “rawcount”, and “results” folders. Run the file using terminal or a Python IDE using the following syntax:

    $ python AnnotationFileCheck.py -[insert parameter]
Example:

    $ python AnnotationFileCheck.py -s -l -p -r -g
    
## Results
Once the script has been run, it will print out errors for the selected parameters and create new error-free versions of the annotation files.
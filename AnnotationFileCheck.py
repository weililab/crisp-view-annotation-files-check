import getopt, sys

def sampleCheck():    
#This program must be in the annotation folder, along with the original sample file
    import os  

#Import regular expression, used to find words regardless of capittalization
    import re

#Check if the sample.txt file exists; if yes, run the annotation check; if no, return "no file"     
    if  (os.path.isfile('annotation/sample.txt') is True):

    #This opens the sample.txt file and makes it readable
        sampleFile = open("annotation/sample.txt", encoding = "ISO-8859-1")
    #Defines the file as a string, and sets it as 'contents' to be used later in the program                                            
        sampleContents = sampleFile.read()
    #Closes the file                                                       
        sampleFile.close()                                                          
        
        print('\n' + "---------------------")
        print("Sample Errors:" + '\n')
    
    #Detects any commas in the sample file and removes them
        if (',' in sampleContents):
            print("0: Comma Error")
            sampleContents = sampleContents.replace(',', '') 
        else:
            print("1: No Comma Error")
    
    #Detects any quotations in the sample file and removes them                                    
        if ('"' in sampleContents):
            print("0: Quotiation Error")
            sampleContents = sampleContents.replace('"', '')
        else:
            print("1: No Quotation Error")
    
    #Detects any capitalization in the 'Type' section and changes it to lowercase                                              
        if (('\tKO\t' or '\tKo\t' or '\tkO\t' or '\tA\t' or '\tI\t' or '\tRI\t' or '\tRI\t' or '\trI\t') in sampleContents):
            print("0: Capitalization Error in Type")
            sampleContents = sampleContents.replace('\tKO\t', '\tko\t')     
            sampleContents = sampleContents.replace('\tKo\t', '\tko\t')
            sampleContents = sampleContents.replace('\tkO\t', '\tko\t')
            sampleContents = sampleContents.replace('\tA\t', '\ta\t')
            sampleContents = sampleContents.replace('\tI\t', '\ti\t')
            sampleContents = sampleContents.replace('\tRI\t', '\tri\t')
            sampleContents = sampleContents.replace('\tRI\t', '\tri\t')
            sampleContents = sampleContents.replace('\trI\t', '\tri\t')
        else:
            print("1: No Capitalization Error in Type")
            
        if re.search('knockout', sampleContents, re.IGNORECASE):
            print("0: Label error in Type")
            sampleContents = sampleContents.replace('\tknockout\t', '\tko\t')
            sampleContents = sampleContents.replace('\KNOCKOUT\t', '\tko\t')
        else:
            print()

        
    #Splits the library file into a list of characters. 
    #If the Unicode character number is creater than the normal 127 character set of ASCII it removes the character from the list
    #Joins the list back into a string after removal
        charErrorNum = 0
        def split(samFile):
            return[char for char in samFile]
        samFile = sampleContents
        sampleList = list(split(samFile))          
        for i in range(0, len(sampleContents)):
            if(ord(sampleList[i]) > 127):
               charErrorNum = charErrorNum+1 
               sampleList[i] = ""
        sampleContents = ''.join(sampleList)
        print("Special Character Error: " + str(charErrorNum))  
            
#THIS CODE IS REPEATED AS THIS TIME IT IS RUN AS IF THERE IS NO ENCODING ERROR
        #Detects any redundant rows and removes them. Also prints a counter for how many redundant rows there are
        sampleNumberRows = 0
        sampleEmptyLines = 0
        sample_contents_fixed = sampleContents.split("\n")
        sample_non_empty_lines = [line for line in sample_contents_fixed if line.strip() != ""]
        sample_contents_without_empty_lines = ""
        for line in sample_non_empty_lines:
            sampleEmptyLines = sampleEmptyLines + 1
            sample_contents_without_empty_lines += line + "\n"
        for line in sample_contents_fixed:
            sampleNumberRows = (sampleNumberRows + 1)
        print("redudant rows: " + str(sampleNumberRows - sampleEmptyLines))
    
        #Creates a new fixed text file   
        sampleFixed = open("annotation/sampleFixed.txt", "w")
        sampleFixed.write(sample_contents_without_empty_lines)
        sampleFixed.close()
    
        #Removes the annotation folder and the file name from the directory    
        newFile = '/annotation/sampleFixed.txt'                                            
        newFilePath  = os.path.realpath(sampleFixed.name)
        if newFile in newFilePath:
            newFilePath = newFilePath.replace(newFile, '')
        #Converts the directory from a string into a list & splits it by folder    
        def Convert(string):
         	li = list(string.split("/"))
         	return li 
        str1 = newFilePath
        test_list = Convert(str1)
                                         
        #Prints the last part in the list, which is  the main directory  
        for i in range(0, len(test_list)):                                             
            if i == (len(test_list)-1): 
                directory = str('\t' + test_list[i-1] + '/' + test_list[i] + '\t')
                #print(directory)
        
        #Detects if the directory acquired from the list is in the sample file, if not, outputs an error
        if directory not in sample_contents_without_empty_lines:
            print("0: Incorrect Directory Error")
        else:
            print("1: No Directory Error")  
    
        #Compares the sample.txt file and sampleFixed.txt file and outputs the lines with errors to errorLines.txt file    
        with open('annotation/sample.txt', 'r') as file1:
            with open('annotation/sampleFixed.txt', 'r') as file2:                                                         
                sampleDifference = set(file1).difference(file2)
        sampleDifference.discard('\n')
        print('\n' + "These lines contain errors:")
        for line in sampleDifference:
           print(line)
        print("---------------------")
    
    else:
        print("No sample.txt file to check")

def libraryCheck():    
    import os
    
    #Import regular expression, used to find words regardless of capittalization
    import re

#Check if the library.txt file exists; if yes, run the annotation check; if no, return "no file"     
    if  (os.path.isfile('annotation/library.txt') is True):                                                                 
    #This opens the library.txt file and makes it readable
        libraryFile = open("annotation/library.txt", encoding = "ISO-8859-1")
    #Defines the file as a string, and sets it as 'contents' to be used later in the program                                            
        libraryContents = libraryFile.read()
    #Closes the file                                                       
        libraryFile.close()                                                          
        
    
        print("Library Errors:" + '\n')
    
    #Detects any commas in the library file and removes them
        if (',' in libraryContents):
            print("0: Comma Error")
            libraryContents = libraryContents.replace(',', '') 
        else:
            print("1: No Comma Error")
    
    #Detects any quotations in the library file and removes them                                    
        if ('"' in libraryContents):
            print("0: Quotiation Error")
            libraryContents = libraryContents.replace('"', '')
        else:
            print("1: No Quotation Error")
    
    
    #Detects any capitalization in the 'Type' section and changes it to lowercase                                              
        if (('\tKO\t' or '\tKo\t' or '\tkO\t' or '\tA\t' or '\tI\t' or '\tRI\t' or '\tRI\t' or '\trI\t') in libraryContents):
            print("0: Capitalization Error in Type")
            libraryContents = libraryContents.replace('\tKO\t', '\tko\t')     
            libraryContents = libraryContents.replace('\tKo\t', '\tko\t')
            libraryContents = libraryContents.replace('\tkO\t', '\tko\t')
            libraryContents = libraryContents.replace('\tA\t', '\ta\t')
            libraryContents = libraryContents.replace('\tI\t', '\ti\t')
            libraryContents = libraryContents.replace('\tRI\t', '\tri\t')
            libraryContents = libraryContents.replace('\tRI\t', '\tri\t')
            libraryContents = libraryContents.replace('\trI\t', '\tri\t')
        else:
            print("1: No Capitalization Error in Type")
            
        if re.search('knockout', libraryContents, re.IGNORECASE):
            print("0: Label error in Type")
            libraryContents = libraryContents.replace('\tknockout\t', '\tko\t')
            libraryContents = libraryContents.replace('\KNOCKOUT\t', '\tko\t')
        else:
            print()
        
    #Splits the library file into a list of characters. 
    #If the Unicode character number is creater than the normal 127 character set of ASCII it removes the character from the list
    #Joins the list back into a string after removal
        charErrorNum = 0
        def split(libFile):
            return[char for char in libFile]
        libFile = libraryContents
        libraryList = list(split(libFile))          
        for i in range(0, len(libraryContents)):
            if(ord(libraryList[i]) > 127):
               charErrorNum = charErrorNum+1 
               libraryList[i] = ""
        libraryContents = ''.join(libraryList)
        print("Special Character Error: " + str(charErrorNum))  

        #Detects any redundant rows and removes them. Also prints a counter for how many redundant rows there are
        libraryNumberRows = 0
        libraryEmptyLines = 0
        library_contents_fixed = libraryContents.split("\n")
        library_non_empty_lines = [line for line in library_contents_fixed if line.strip() != ""]
        library_contents_without_empty_lines = ""
        for line in library_non_empty_lines:
            libraryEmptyLines = libraryEmptyLines + 1
            library_contents_without_empty_lines += line + "\n"
        for line in library_contents_fixed:
            libraryNumberRows = (libraryNumberRows + 1)
        print("redudant rows: " + str(libraryNumberRows - libraryEmptyLines))
    
        #Creates a new fixed text file   
        libraryFixed = open("annotation/libraryFixed.txt", "w")
        libraryFixed.write(library_contents_without_empty_lines)
        libraryFixed.close()  
    
        #Compares the library.txt file and libraryFixed.txt file and outputs the lines with errors to errorLines.txt file    
        with open('annotation/library.txt', 'r') as file1:
            with open('annotation/libraryFixed.txt', 'r') as file2:                                                         
                libraryDifference = set(file1).difference(file2)
        libraryDifference.discard('\n')
        print('\n' + "These lines contain errors:")
        for line in libraryDifference:
           print(line)
        print("---------------------")
        
    else:
        print("No library.txt file to check")
        print("---------------------")


def publicationCheck():    
#This program must be in the annotation folder, along with the original publication file
    import os
    
#Check if the library.txt file exists; if yes, run the annotation check; if no, return "no file"     
    if  (os.path.isfile('annotation/publication.txt') is True):                                                                     
    #This opens the publication.txt file and makes it readable
        publicationFile = open('annotation/publication.txt', encoding = "ISO-8859-1")
    #Defines the file as a string, and sets it as 'contents' to be used later in the program                                            
        publicationContents = publicationFile.read()
    #Closes the file                                                       
        publicationFile.close()                                                          
        
        print("Publication Errors:" + '\n')
    
    #Detects any commas in the publication file and removes them
        if (',' in publicationContents):
            print("0: Comma Error")
            publicationContents = publicationContents.replace(',', '') 
        else:
            print("1: No Comma Error")
    
    #Detects any quotations in the publication file and removes them                                    
        if ('"' in publicationContents):
            print("0: Quotiation Error")
            publicationContents = publicationContents.replace('"', '')
        else:
            print("1: No Quotation Error")
        
        print()
        
    #Splits the Publication file into a list of characters. 
    #If the Unicode character number is creater than the normal 127 character set of ASCII it removes the character from the list
    #Joins the list back into a string after removal
        charErrorNum = 0
        def split(pubFile):
            return[char for char in pubFile]
        pubFile = publicationContents
        publicationList = list(split(pubFile))          
        for i in range(0, len(publicationContents)):
            if(ord(publicationList[i]) > 127):
               charErrorNum = charErrorNum+1 
               publicationList[i] = ""
        publicationContents = ''.join(publicationList)
        print("Special Character Error: " + str(charErrorNum))    
        

        #Detects any redundant rows and removes them. Also prints a counter for how many redundant rows there are
        publicationNumberRows = 0
        publicationEmptyLines = 0
        publication_contents_fixed = publicationContents.split("\n")
        publication_non_empty_lines = [line for line in publication_contents_fixed if line.strip() != ""]
        publication_contents_without_empty_lines = ""
        for line in publication_non_empty_lines:
            publicationEmptyLines = publicationEmptyLines + 1
            publication_contents_without_empty_lines += line + "\n"
        for line in publication_contents_fixed:
            publicationNumberRows = (publicationNumberRows + 1)
        print("redudant rows: " + str(publicationNumberRows - publicationEmptyLines))
    
        #Creates a new fixed text file   
        publicationFixed = open("annotation/publicationFixed.txt", "w")
        publicationFixed.write(publication_contents_without_empty_lines)
        publicationFixed.close()  
    
        #Compares the publication.txt file and publicationFixed.txt file and outputs the lines with errors to errorLines.txt file    
        with open('annotation/publication.txt', encoding = "ISO-8859-1") as file1:
            with open('annotation/publicationFixed.txt', 'rt') as file2:                                                         
                publicationDifference = set(file1).difference(file2)
        publicationDifference.discard('\n')
        print('\n' + "These lines contain errors:")
        for line in publicationDifference:
            print(line)
        print("---------------------")

    else:
        print("No publication.txt file to check")

def resultsCheck():
#Goes through the results folder to check if the mle.visper.yaml file exists   
    import os
    
    print("Results Folder Errors:" + '\n')
#Check if the mle.vispr.yaml file exists; if yes, run the annotation check; if no, return "no file"     
    if  (os.path.isfile('results/mle.vispr.yaml') is True):   
        print("1: No Results Folder Error, mle.vispr.yaml File Found")
    else:
        print("0: Results Folder Error, mle.vispr.yaml File Not Found")
    print("---------------------") 
    
def generalFileCheck():
#Checks to see if rawcount, fastq, and lbrary files exist    
    import os
#Check if the raw count file is there, if not there check if fastq is there. If no fastq, report error
    print("General File Errors:" + '\n')
    if  (os.path.isfile('rawcount/rawcount.txt') is True):   
        print("1: No Rawcount Error, Rawcount File Found")
    else:
        if(os.path.isdir('rawcount/') is True):
            if any(File.endswith(".fastq") for File in os.listdir("rawcount/")):
                print("1: No Rawcount File Not Found, Fastq File Found")
        else:
            print("Rawcount Error, No Rawcount or Fastq File Found")
#Check if Library file exists
    if  (os.path.isfile('lib/library.csv') is True):   
        print("1: No Library Error, Library File Found")
    else:           
        print("0: Library Error, No Library File Found")
    print("---------------------") 

#Remove first argument from the list of command line arguments
argumentList = sys.argv[1:]

#Options
options = "slprg"


try:
    #Parsing argument
    arguments, values = getopt.getopt(argumentList, options)

    #Checking each argument
    for currentArgument, currentValue in arguments:
        
        if currentArgument in ("-s"):
            sampleCheck()
        
        elif currentArgument in ("-l"):
            libraryCheck()
            
        elif currentArgument in ("-p"):
            publicationCheck()
        
        elif currentArgument in ("-r"):
            resultsCheck()
        
        elif currentArgument in ("-g"):
            generalFileCheck()
            
except getopt.error:
    #Output error, and return with an error code
    print("error in argument file type")